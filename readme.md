にんぎょう
=====

Ningyou ("Doll") is a simple IRC bot written in Ruby. Most functionality comes
in the form of "plugins", which are loaded when the progam first starts up.

No special gems are required for the bot itself, though plug-ins may depend on
some. Additionally, the [HTMLEntities](https://rubygems.org/gems/htmlentities)
gem is used if it is available, but this is purely optional.

Public Commands
--------------

Public commands must be prefixed with `nick:` or `nick,` to be detected.

### plugins

The bot will respond to `botnick: plugins` with a list of any active plugins.

To use a plug-in, send a public message in the format `botname: plugin params`

### blahblahblah?

Any (non-plug-in) command that ends with a question mark will be treated as a
"magic 8-ball" question, with a random response given as a reply.

Private Message Commands
------------------------

Commands sent via PM are used as "passwords" to make the bot do certain things.

### The "op me" password

This will cause the bot to set operator status on whoever sent the PM, assuming
that it has permission to do so.

### The "quit irc" password

This will cause the bot to quit IRC and stop running.

How do plug-ins work?
---------------------

A plug-in is just a Ruby file in the `./plugins/` directory, with the following
characteristics:

+ The file must define a class with same name as the file, minus the `.rb` bit.
  For example, a file called `MyPlugin.rb` should contain a `MyPlugin` class.
+ The class must have a `run()` method which accepts a single string parameter
  and returns a string as a result.

The command to trigger the plug-in via IRC will be the same as the class name.

TODO List
---------

+ Load settings from a file.
+ More plugins!

License
-------

Copyright (c) 2014 Thomas Glyn Dennis

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
