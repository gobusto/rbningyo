#!/usr/bin/ruby
=begin
A basic random number/coin toss plugin.

This file is licensed as CC0: http://creativecommons.org/publicdomain/zero/1.0/
=end

class Random

  def run(params)
    return "Tossed a coin: " + ["Heads", "Tails"][rand(2)] if params == 'coin'
    x = params.to_i
    return x > 1 ? (rand(x) + 1).to_s : "Usage: Random [coin|2+]"
  end

end
