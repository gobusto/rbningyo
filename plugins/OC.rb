#!/usr/bin/ruby
=begin
A random "original character" generator (Dedicated to Edd B)

This file is licensed as CC0: http://creativecommons.org/publicdomain/zero/1.0/
=end

class OC

  def initialize
    @species = ["Hedgehog", "Vampire", "Ninja", "Ghost", "Data Analyst", "Fig",
      "Slug", "Cyberdemon", "Pirate", "Robot", "Pornographer", "Zombie", "Cat",
      "Accountant", "Tambourine", "Alien", "artist formerly known as Prince"]
    @adjective = ["Super", "Hyper", "Mega", "Ultra", "Turbo", "Magic", "Giga",
      "Punctual", "Erogenous", "Invisible", "Killer", "Secret", "Blue", "Red",
      "Pretty", "Kawaii", "Sexy", "Dickensian", "Marxist", "Conservative"]
    @attack = ["Beam", "Sword", "Laser", "Ray", "Spear", "Cannon", "Axe", "Car",
      "Spaceship", "Computer", "Frisbee", "Potato", "Spoon", "C Compiler", "IQ",
      "Gun", "Ballista", "Jagdpanther", "Robot", "Stick", "Taxi", "Submarine"]
    @effect = ["inconvenience", "obliterate", "seriously injure", "irritate",
      "shrink", "arouse", "disinfect", "barely effect", "never seem to kill",
      "atomise", "liquify", "remotely detonate", "consume", "destroy", "help"]
    @power = ["a whole kitchen", "just one guy", "absolutely no-one", "Brian",
      "John Romero", "sharks", "puddles", "Belgian nationals", "David Cameron",
      "the entire galaxy", "cities", "continents", "an area the size of a pea"]
    @home = ["a secret moon base", "Portugal", "an underground lair", "Leeds",
      "an invisible sky fortress", "a tree", "the sea", "Stockport", "a cave",
      "another dimension", "an ordinary house", "a constant state of fear"]
    @food = ["burgers", "cakes", "pocky", "ramen", "pizza", "chili dogs", "poo",
      "apples", "Cheese Moments", "pencils", "bacon", "ice cream", "sausages",
      "vegetarian food", "human flesh", "raw fish", "only the finest foods"]
    @music = ["NiN", "Avril Lavigne", "Reel Big Fish", "U2", "Radiohead",
      "Aphex Twin", "Miley Cyrus", " the voices in their head", "the radio",
      "Korn", "Big Band Jazz", "Classical Music", "techno remixes of pop music"]
  end

  def run(params)
    params + " the " + get(@adjective) + " " + get(@species) + " uses their " +
        get(@adjective) + " " + get(@adjective) + " " + get(@attack) +
        ", which can " + get(@effect) + " " + get(@power) +
        ", to fight the bad guys. They live in " + get(@home) +
        ", eat " + get(@food) + " and listen to " + get(@music) +
        ". (ORIGINAL CHARACTER, DO NOT STEAL)"
  end

  def get(the_list) the_list[rand(the_list.length)] end

end
