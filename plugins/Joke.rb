#!/usr/bin/ruby
=begin
Random Jokes

This file is licensed as CC0: http://creativecommons.org/publicdomain/zero/1.0/
=end

class Joke

  def initialize
    @terrible_jokes = [
      "A man walks into a bar... ouch!",
      "An Englishman, an Irishman, and a Scotsman walk into a bar. The barman says: 'Is this a joke?'",
      "Did you hear about the comedian who made his own beer? It was a barrel of laughs.",
      "Did you hear about the joke that doesn't have a punchline?",
      "Did you hear about the man who invented a blue banana? He spent the rest of his life trying to cheer it up.",
      "Did you hear about the skeleton detective? His name was Sherlock Bones.",
      "Doctor, doctor, I think I'm a pair of curtains. / Oh, pull yourself together!",
      "Doctor, doctor, I think I'm upside down / Why? / Because my nose runs and my feet smell.",
      "Hens are great comedians; they're always cracking eggcelent yolks.",
      "How did the archaeologist know how to enter the Egyptian tomb? A sign said: 'Toot an come in'",
      "How do you make a sugar puff? Chase it around the garden.",
      "How do you make a swiss roll? Push him down a hill.",
      "I bought you a wombat. / What's that good for? / A good old game of Wom.",
      "I saw a woman with a broom near the road the other day with her thumb up; probably a witch-hiker.",
      "Knock Knock/Who's there?/Boo/Boo who?/Don't cry, it's only a joke.",
      "Knock Knock/Who's there?/Irish Stu/Irish Stu who?/Irish Stu in the name of the law.",
      "Knock Knock/Who's there?/Ivan/Ivan who?/I van't to come in!",
      "Knock Knock/Who's there?/Twit Too/Twit Too who?/Wow, are you an owl?",
      "What's the best form of transport under the sea? A motorpike with a sidecarp.",
      "What did the daddy telephone say to the child telephone? 'You're too young to be engaged.'",
      "What did the policeman say to his stomach? 'You're under a vest.'",
      "What did the sea say to the penguin? Nothing - it just waved.",
      "What do snowmen sing at parties? Freeze a jolly good fellow.",
      "What do snowmen ride to work? Ice-cycles.",
      "What do lawyers wear? Lawsuits.",
      "What do you call a dinosaur with one eye? A Dyerthinkhesaurus.",
      "What do you call a gun with three barrels? A trifle.",
      "What do traffic wardens have on their sandwiches? Traffic jam.",
      "What do you call a man with a spade in his head? Doug.",
      "What do frogs like to drink? Croak-a-cola.",
      "What film do ducks watch? Quack to the future.",
      "What game do you play with a crocodile? Snap.",
      "What happens if you plug a cat into your PC? It becomes a purr-inter.",
      "Where can you find a star fish? In a movie studio.",
      "Where did the general keep his armies? Up his sleevies.",
      "Where do fish keep their cash? In a river bank.",
      "Where do penguins keep their cash? In a snow bank.",
      "Why are clocks good at filling in forms? They're constantly ticking.",
      "Why are frogs the most knowledgeable animal? Because for every book they say 'ReadIt, ReadIt, ReadIt'",
      "Why are railway workers so athletic? Because they train every day.",
      "Why did the baby biscuit cry? Because its mother was a wafer so long.",
      "Why did the one-armed man cross the road? To get to the second hand shop.",
      "Why did the shore blush? Because the sea weed.",
      "Why didn't the skeleton go the party? He had no body to go with."]
  end

  def run(params) return @terrible_jokes[rand(@terrible_jokes.length)] end

end
