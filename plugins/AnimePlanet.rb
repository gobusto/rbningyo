#!/usr/bin/ruby
=begin

Search Anime-Planet using the http://ogp.me metadata it provides for each page.

Copyright (c) 2014 Thomas Glyn Dennis

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

=end

require 'nokogiri'  # http://www.nokogiri.org
require 'open-uri'
require 'uri'

class AnimePlanet

  def run(phrase)

    # TODO: Support 'characters ' too, handling the page format differences...
    if not phrase.start_with? 'anime ', 'manga '
      return 'AP search format: [anime|manga] search text [#resultnumber]'
    end

    # If multiple results are returned, the user can specify one.
    category, phrase = phrase.split(' ', 2)
    phrase, tmp, r_id = phrase.partition('#')

    # Query the site and return the results.
    url = 'http://www.anime-planet.com/' + category + '/all?name='
    text = get(url + URI::encode(phrase).gsub('%20', '+'), r_id.to_i - 1)
    return text if text.strip != ''
    return "Sorry, I couldn't find any information about " + phrase

  end

  def get(url, num)

    # Use Nokogiri to parse the HTML data returned by the website.
    begin
      html = Nokogiri::HTML(open(url))
    rescue
      return 'Oh dear, something went wrong: ' + $!.to_s
    end

    # Get the OpenGraph metadata, including a link to the actual page.
    title = html.css("meta[property='og:title']")
    about = html.css("meta[property='og:description']")
    url = html.css("meta[property='og:url']")
    results = html.css("a[data-class='tooltip-full']")  # For multiple matches.

    if about.length > 0
      # Exact match: Get the text from the 'description' meta tag.
      t = about[0]['content'].to_s
      t = 'No description is available.' if t == ''
      return title[0]['content'].to_s + ": #{t} -- " + url[0]['content'].to_s
    elsif results.length > 0
      # Multiple search results...
      if num >= 0 and num < results.length
        # If a specific result index is provided, redirect to that one.
        return get('http://www.anime-planet.com' + results[num]['href'].to_s, 0)
      else
        # Otherwise, just list the possibilities.
        text = 'I found ' + results.length.to_s + ' possible matches:'
        results.length.times do |i|
          text += ', "' + (i + 1).to_s + '. ' + results[i].inner_html.to_s + '"'
        end
        return text.sub(',', '')  # Get rid of the first comma.
      end
    end

    # Nothing was found.
    return ""

  end

end
