#!/usr/bin/ruby
=begin

A simple Wikipedia search for Ruby, using the official JSON-based API.

Copyright (c) 2014 Thomas Glyn Dennis

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

=end

require 'nokogiri'  # http://www.nokogiri.org
require 'open-uri'
require 'uri'
require 'json'

class Wikipedia

  def run(phrase)
    return "You need to specify something to search for." if phrase.strip == ''

    # http://stackoverflow.com/questions/8555320/
    begin
      url = 'http://en.wikipedia.org/w/api.php?format=json&action=query'
      url += '&prop=revisions&rvprop=content&rvsection=0&rvparse=&redirects='
      json = JSON.parse(open(url + '&titles=' + URI::escape(phrase)).read)
    rescue
      return 'Oh dear, something went wrong: ' + $!.to_s
    end

    # Attempt to extract the results, if they exist.
    begin
      html = ''
      for page in json['query']['pages'].values
        html = Nokogiri::HTML(page['revisions'][0]['*'])
      end
    rescue
      return "Sorry, I couldn't find any information about " + phrase
    end

    # Extract the important bits from the HTML and return it as a string.
    return get_text(html)

  end

  ## Recursively search for white-listed HTML element types and get their text.
  def get_text(html)
    text = ''
    for item in html.children
      if item.text?
        text += item.content
      elsif ['html', 'body', 'a', 'b', 'i', 'p'].count(item.name) > 0
        text += get_text(item)
      end
    end
    return text
  end

end
