#!/usr/bin/ruby
=begin

NINGYOU - A simple IRC bot.

Copyright (c) 2014 Thomas Glyn Dennis

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

=end

require 'socket'

# TODO: These should be loaded in from a configuration file or similar.

PASSWORD_QUIT = 'please leave'
PASSWORD_OP = 'please op me'

server = 'irc.freenode.org'
channel = '#channelnamegoeshere'
nick = 'irctestuser'

# =============================================================================

begin
  require 'htmlentities'  # https://rubygems.org/gems/htmlentities
rescue LoadError
  require 'cgi'  # This can be used as a fallback.
end

# This function cleans up any special HTML characters.
def cleanHTML(text)
  # Replace any HTML character entity references with the correct characters.
  begin
    return HTMLEntities.new.decode(text)
  rescue NameError
    # CGI::unescapeHTML() only handles XML entities, so add a few common cases.
    text = text.gsub('&lsquo;', "'").gsub('&rsquo;', "'")
    text = text.gsub('&ldquo;', '"').gsub('&rdquo;', '"')
    return CGI::unescapeHTML(text).gsub('&nbsp;', ' ').gsub('&mdash;', '-')
  end
end

# =============================================================================

# This function breaks long messages into multiple parts.
def sendMessage(socket, sender, target, message, send_as_action)
  line_start = 'PRIVMSG ' + target + ' :'
  line_end = ''
  if send_as_action
    line_start += "\01ACTION "
    line_end += "\01"
  end

  # Calculate the basic length of a single line.
  tmp = ':' + sender + '!~123456789@' + Socket.gethostname + ' ' + line_start
  base_length = (tmp + line_end + ' ' + sender).length

  # Keep adding words until the maximum line length is ALMOST reached...
  line = ''
  for word in message.split + ['']
    new_line = (line == '') ? word : (line + ' ' + word)
    if word == '' or base_length + new_line.length > 510
      # FIXME: This assumes that the CURRENT line isn't over the limit...
      socket.puts line_start + line + line_end
      line = word
    else
      line = new_line
    end
  end

end

# =============================================================================

# Load plugins.
plugins = {}
Dir::foreach('./plugins') do |item|
  next if not item.end_with? '.rb'
  require './plugins/' + item
  basic_name = item.chomp('.rb')
  plugins[basic_name] = eval(basic_name + '.new')
end

# See RFC 1459 / 4.1 Connection Registration
s = TCPSocket.new(server, 6667)
s.puts "NICK %s" % nick
s.puts "USER %s %s * :I'm a bot!" % [nick, Socket.gethostname]

# Loop until 'nil' is returned.
while line = s.gets

  # Only lines starting with a colon character include a prefix.
  if line.start_with? ':'
    prefix, tmp, line =  line.sub(':', '').partition(' ')
  else
    prefix = ''
  end

  # The rest of the line is in the format: COMMAND arg1 ... argn\r\n
  line, sep, trailing = line.strip.partition(':')
  params = line.split
  params.push trailing if trailing != ''

  # Output the line data for debugging purposes.
  puts Time.new.to_s + ' ' + prefix + ' > ' + params.to_s

  # Handle known commands.
  if params[0] == 'ERROR'
    break
  elsif params[0] == 'PING'
    s.puts 'PONG :' + params[1]  # forums.unrealircd.org/viewtopic.php?t=2098
  elsif params[0] == '376'  # End of MOTD
    s.puts "JOIN " + channel
  elsif params[0] == 'PRIVMSG'

    if params[1] == nick

      # Private messages are used to send password-protected commands.
      if params[-1] == PASSWORD_QUIT
        break  # Stop the 'while' loop and terminate the program.
      elsif params[-1] == PASSWORD_OP
        s.puts 'MODE ' + channel + ' +o ' + prefix.split('!')[0]
      end

    elsif params[-1].start_with? nick + ':', nick + ','

      # Public messages (prefixed with the nick) are used for anything else.
      instruction_text = params[-1].sub(nick, '')  # Remove the nick...
      instruction_text[0] = ''  # ...and the ',' or ':' character...
      instruction_text.strip!  # ...and any spaces.

      # First of all, try looking for a plugin to deal with this command.
      responded_to_message = false
      for key in plugins.keys
        if instruction_text == key or instruction_text.start_with? key + ' '
          text = plugins[key].run(instruction_text.partition(' ')[2])
          sendMessage(s, nick, channel, cleanHTML(text), false)
          responded_to_message = true
        end
      end

      # If no suitable plugin was found, just give a basic response.
      if not responded_to_message
        if instruction_text == 'plugins'
          sendMessage(s, nick, channel, plugins.keys.to_s, false)
        elsif instruction_text.end_with? '?'
          answers = ['Yes.', 'No.', 'Maybe?', 'yep', 'lol nah', 'idunnolol']
          sendMessage(s, nick, channel, answers[rand(answers.length)], false)
        else
          sendMessage(s, nick, channel, "doesn't understand.", true)
        end
      end

    end

  end

end

# Send a quit message so that the server knows to close the connection and end.
s.puts 'QUIT'
s.close
